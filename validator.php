<?php

class Validator{

    public $errors = array();

    public function isNumber($value){
        if(!is_numeric($value)){
            $this->errors['number'] = 'Not a number!';
        }

        return $this;
    }

    public function maxLenght($value, $max){
        if(strlen($value) > $max){
            $this->errors['max'] = 'Bigger then maximum!';
        }

        return $this;
    }

    public function minLenght($value, $min){
        if(strlen($value) < $min){
            $this->errors['min'] = 'Less then minimum!';
        }

        return $this;
    }

    public function AlphaNumeric($value){
        if(!ctype_alnum($this)){
            $this->errors['AlphaNumeric'] = 'Not correct!';
        }

        return $this;
    }

    public function correctEmail($value){
        if(empty(filter_var($value, FILTER_VALIDATE_EMAIL))){
            $this->errors['email_correct'] = 'Email is not correct!';
        }

        return $this;
    }

    public function lengthValidate($value, $min, $max){
        if(strlen($value) > $max || strlen($value) < $min){
            $this->errors['length'] = 'Length is not correct!';
        }

        return $this;
    }

    public function isRequired($value){
        if(empty($value)){
            $this->errors['empty'] = 'Empty form';
        }

        return $this;
    }

    public function passMatch($value, $value_confirm){
        if(!($value === $value_confirm)){
            $this->errors['passMatch'] = 'Password doesnt match';
        }

        return $this;
    }

    public function isArray($value){
        if(is_array($value)){
            $this->errors['array'] = 'Cant use array!';
        }

        return $this;
    }

    public function isURL($value){
        if(!filter_var($value, FILTER_VALIDATE_URL)){
            $this->errors['url'] = 'Cant use url!';
        }

        return $this;
    }
}